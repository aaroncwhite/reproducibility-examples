import os

import my_great_app

SECRET_CODE = os.environ.get('SECRET_CODE', 'No secret code found! Try adding SECRET_CODE="your_code" to the .env file.')

def write_file():
    """Writes a file with the SECRET_CODE environment
    variable and names the file with the current 
    app version
    """
    out_dir = 'example_outputs'

    if not os.path.exists(out_dir): 
        os.makedirs(out_dir)

    # Make a filename called example_report_VERSION.txt in the output directory
    fname = os.path.join(out_dir, 'example_report_{version}.txt'.format(version=my_great_app.__version__))

    with open(fname, 'w') as f:
        f.write(SECRET_CODE)
        f.write('\n\n')
        f.write("MPR Example Report produced with code version: {version}".format(version=my_great_app.__version__))

    print("New report created at {}".format(fname))



if __name__ == "__main__":
    print("Hey, I'm currently running on version {version}".format(version=my_great_app.__version__))

    print(SECRET_CODE)
    print()
    print("===========================================")
    print("Generate 'report' example")
    print("===========================================")

    write_file()