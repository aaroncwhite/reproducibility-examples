from luigi import build

from my_great_app.tasks import ProcessData

if __name__ == '__main__':
    build([ProcessData()], local_scheduler=True)