# Reproducibility Examples

I've started collecting examples that I use when training other data scientists on good practices.  This repository collects a few different trainings I've put together as well as a working example for pipenv, git flow, and luigi.

Presentations:
* [Environments](presentations/1_Machines.md)
* [Versioning](presentations/2_Time.md)

Luigi:
* Tasks can be found in [my_great_app](my_great_app/tasks) and run with `pipenv run python luigi_example.py`.

# Working Example

## Pipenv
I've included a `Pipfile` and `Pipfile.lock` in this folder.  Try the following commands to see how using `pipenv` changes the environment. 

First install pipenv:
```
pip install pipenv
```

Install the most recent version of every package using `pipenv`:
```
pipenv install
```

Take a look at the [`Pipfile`](Pipfile).  Can you run `pipenv run pytest`?

We didn't install the development dependencies, which are separate in `pipenv` to reduce the cruft if just deploying an application. 
```
pipenv install --dev --ignore-pipfile
```

This will use the [`Pipfile.lock`](Pipfile.lock) file to install dependencies.  This is a much more specific and declarative definition of your last known working configuration.

Now try `pipenv run pytest` or `pipenv run jupyter lab`. Should work!

Tips vs conda:
Activating an environment in conda

Conda works like this
```
source activate my-env
```

Pipenv can combine the activation and running into one command:
```
pipenv run python
```
Or, you can activate it just like a conda environment
```
pipenv shell
```

## Git flow and Versioning
Try running `pipenv run python main.py` and see what version is displayed.  Change the branch and try it again. 

Add a new semantic versioned tag to the repo.  Notice how the version changes!

Take a look in `example_reports` after you've run `main.py` a few times.  You should have a 'report' that includes the version of the code used in the file name as well as in the body of the file.  This is one example of how we can integrate versioning directly into python code.

### Try out git flow
Check out the commands based on the [GitHub README](https://github.com/nvie/gitflow)

Setup git flow first:
```
git flow init
```
Use the defaults for now. 

Try making a `feature` branch:
```
git flow feature start a-new-feature --showcommands
touch my_great_app/new_file.py
git add my_great_app/new_file.py
git commit -m "feat: add a great new file"
git flow feature finish --showcommands
```
Feel free to actually add something to that `new_file.py`. `--showcommands` isn't necessary, but will display the git commands that are run under the hood. 

Now check out the graph:
```
git log --all --decorate --oneline --graph
```

Compare that to only commiting on one branch.  Switch to master and make a few commits.
```
git checkout master
touch my_great_app/file1.py
git add my_great_app/file1.py
git commit -m "bad commit 1"

touch my_great_app/file2.py
git add my_great_app/file2.py
git commit -m "bad commit 2"

touch my_great_app/file3.py
git add my_great_app/file3.py
git commit -m "bad commit 3"
```

Look at the graph again:
```
git log --all --decorate --oneline --graph
```

Like the graph?  Add it to your `~/.bashrc` and make things easier:
```
echo 'alias git-graph="git log --all --decorate --oneline --graph"' >> ~/.bashrc
source ~/.bashrc
```

One more time:
```
git-graph
```

### Release your changes
Git flow will bake the tag for you if you name your release branch accordingly.  Try releasing a new version:

Get the current tags:
```
git tag
```

Versions can take the form of `MAJOR.MINOR.PATCH`.  See more at [semver.org](https://semver.org).  Let's assume you've done enough for a minor bump.

Add 1 to the minor version and start a release.  My example currently is `0.3.0` so I will release `0.4.0`:
```
git flow release start 0.4.0 --showcommands
```

Now's a good time to update the [CHANGELOG.md](CHANGELOG.md) file if you haven't already!  Commit any last minute changes.

```
git flow release finish --showcommands
```

After finishing, you should be back on `develop` and have a new tag on your repository.  

## Env variables
Try adding a file called `.env` in the root directory. Make a variable called `SECRET_CODE` with your favorite message. 

```
SECRET_CODE="Dwight, at 8am someone poisons the coffee.  Repeat, do not drink the coffee. Cordially, Future Dwight"
```

Run `pipenv run python main.py` to see how the secret code message changes. 


## Updating things
Try updating the `write_file()` method in `main.py` to include a new `.env` variable that you define called `MY_SECRET_VAR`.  Print it in the report that is created.  

