from luigi import Task, IntParameter, Parameter, LocalTarget
import pandas as pd
import numpy as np


class GenerateData(Task):

    nrows = IntParameter(default=10, description='How many rows to generate.')
    ncols = IntParameter(default=3, description='How many rows to generate.')

    def output(self):
        return LocalTarget('./raw_data/data.csv')

    def run(self):
        df = pd.DataFrame(np.random.randint(0,100,size=(self.nrows, self.ncols)))
        with self.output().open('w') as f:
            df.to_csv(f)


class ProcessData(Task):

    output_name = Parameter(default='processed')

    def output(self):
        return LocalTarget('./results/{}.csv'.format(self.output_name))

    def requires(self):
        return GenerateData() # Use default parameters

    def run(self):
        with self.requires().output().open() as f:
            d = pd.read_csv(f)

        d = d.apply(lambda x: x**2)

        with self.output().open('w') as f:
            d.to_csv(f)