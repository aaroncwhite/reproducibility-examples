# Changelog

## 0.5.1
--------

* Collect presentations into the same place

## 0.5.0
--------

* Add basic luigi example

## 0.4.0
--------

* Add 'report' generation example using `__version__` based on scm tag. 
* Add suggestion to modify report using a new `.env` variable.


## 0.3.0
--------

* Add great new feature!

## 0.2.0
--------

* Add env example
* Add version example
* Fix directory structure

## 0.1.0 
--------

* Add machine example for using pipenv