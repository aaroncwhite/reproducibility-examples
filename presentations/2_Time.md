---
title: Reproducibility
subtitle: over time
author: Aaron White 
---

# Goal

## Code that produces the expected results

like when you first wrote it

. . .

whether that's a week or a year later.

. . . 

It might be review

# Versioning (Git) 

## Versioning
![](https://imgs.xkcd.com/comics/git.png)

## Version Control
We all know about git

. . .

but we might not take full advantage...

## Bad habits
```
git add -a
git commit -m "made so many changes I don't know what happened"
```

. . .

We've all done it. 

## Atomic Commits
Atomic commits can help us pinpoint exactly when a change caused problems in our code.

. . .

```
$ git commit main.py -m "feat: implement a breaking change"
```

## Atomic Commits
Keeping a history of self-contained changes helps:

* Code reviews
* Easier roll back

Massive commits don't do that!

## Atomic Commits 
Check out [Atomic Commits with Git](https://seesparkbox.com/foundry/atomic_commits_with_git) for a walk through.


## Commit messages
![](https://imgs.xkcd.com/comics/git_commit.png)


## Semantic Commits
Let's write better commit messages for ourselves and for our colleagues

```
feat: add hat wobble
^--^  ^------------^
|     |
|     +-> Summary in present tense.
|
+-------> Type: chore, docs, feat, fix, refactor, style, or test.
```
[Source](https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716)


## Semantic Commits
feat: (new feature for the user, not a new feature for build script)

. . .

fix: (bug fix for the user, not a fix to a build script)

. . .

docs: (changes to the documentation)

. . .

style: (formatting, missing semi colons, etc; no production code change)


## Sematic Commits

refactor: (refactoring production code, eg. renaming a variable)

. . .

test: (adding missing tests, refactoring tests; no production code change)

. . . 

chore: (updating grunt tasks etc; no production code change)

## Branching
Branches can help maintain useable code at all times

. . . 

Branches can also group related commits together

## Branching

At minimum, we want:

* `master` is 'deployable' code at all times.
* `develop` is where all new changes are made.

## Branching
Additional branches are informative and group related work together

`feature/update-indicator-definitions`

`add-new-flashy-widget`

## Branching
But this can cause more problems when we leave branches hanging around.

It's best to merge changes back on to `develop` and `master`.  

## Where are the latest changes?
![](0_assets/branches.png)


## Tags
We can reference specific commit ids to get a specific version of the code, but that isn't very human friendly. 

. . . 

Which is more recent? 

. . .

`32bf224b` 

`a481118ca`

## Tags
Git has a built in tagging system to add human friendly names to a point in time we want to remember. 

. . .

Combined with an always deployable `master` branch, we can tag versions as we upgrade our code in a systematic way. 


# Semantic Versioning

## A structured way to increment code versions
*"Under this scheme, version numbers and the way they change convey meaning about the underlying code and what has been modified from one version to the next."*

[semver.org](https://semver.org)

## Version Components

Given a version number MAJOR.MINOR.PATCH 

increment the:

. . .

MAJOR version for incompatible API changes,

. . .

MINOR version for added functionality

. . . 

PATCH version for backwards-compatible bug fixes.

## When is this useful?

. . . 

Year over year improvements = Major version

. . .

Addition to report = Minor version

. . . 

Fixing error in calculation = Patch version

# Git (work)flow

## Combining it all together
*Git Flow* is a structured approach to git versioning

[Original Post](https://nvie.com/posts/a-successful-git-branching-model/)

## Main branches

<img src="https://nvie.com/img/main-branches@2x.png"  width="350px">

## Master
"always reflects a production-ready state"

## Develop
"reflects a state with the latest delivered development changes for the next release"

## Supporting branches
<img src="https://nvie.com/img/fb@2x.png" width="200px">

## Feature branches
Are ephemeral and cleaned up after work is complete
```
$ git checkout -b feature/something-new
-- make changes --
$ git checkout develop
$ git merge feature/something-new --no-ff
$ git branch -d feature/something-new
```

## Non-fastforward merges
`--no-ff` preserves the branch history without having to keep the branch.

## When to feature?
. . . 

They don't have to match the specs exactly.

. . . 

"Create new data elements and make X, Y, Z images"

. . . 

```
1. feature/add-data-element-calculations
2. feature/add-data-element-plots
3. bump minor version
```

## This sounds hard

. . . 

It's built into git bash!

## Git (bash) flow
Setting up a project:
```
$ git clone my-repo-url
$ git flow init
No branches exist yet. Base branches must be created now.
Branch name for production releases: [master]
```

## Git (bash) flow
Git flow automatically puts you on to the development branch

## New features
Starting a new feature
```
$ git flow feature start my-fancy-new-feature
```

## New features
Finishing a new feature
```
$ git flow feature finish
```
Under the hood, it's running the four commands we showed before.

## Pulling in semantic versions
Name your release branches with semantic versions

and git flow will automatically tag master when you're finished.
```
$ git flow release start 0.1.0
--- update change log ---
--- any last minute bug fixes ---
$ git flow release finish
$ git tag
0.1.0
```

## Visualizing the graph
```
git log --all --decorate --oneline --graph
```

## Is this the only workflow?
No

. . . 

But if you don't follow a good one, this is a great place to start. 

. . .

Just be consistent!

## Other options
[Atlassian Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
[GitHub Flow](https://guides.github.com/introduction/flow/)

# Integrating with Python

## Setuptools-scm
The "blessed" package to manage your versions by scm tags

[GitHub](https://github.com/pypa/setuptools_scm)

## Setuptools-scm
Setuptools-scm can use your git tagged version to automatically add a version to your python code.
```
>>> import my_code_module
>>> print(my_code_module.__version__)
0.1.0
```

## Dirty tags
If you are not on master, it assumes you're doing a bug fix, will show the branch you're on, how many commits ahead of master, plus the commit hash. 
```
>>> import my_code_module
>>> print(my_code_module.__version__)
'0.1.1.dev38+g94b6726'
```

## When can we use it?
Easy traceability back to specific reports.

. . . 

Bake into reports, outputs, etc.


