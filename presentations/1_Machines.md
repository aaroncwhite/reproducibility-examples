---
title: Reproducibility
subtitle: across machines
author: Aaron White
---

# Machine Environments

## We've all been here
![](https://imgs.xkcd.com/comics/python_environment.png)

## Don't do this
```
$ python
```
```
C:\Users\myhome\> python
```

## PLEASE __*NEVER*__ do this
```
$ pip install some-package-i-need
```
```
C:\Users\myhome\> pip install some-package-i-need
```

## What could go wrong?
. . .

```
pip install some-package-i-need
```
Runs the `setup.py` file for the package

. . .

Yes there are things like wheels, etc. 

## What could go wrong?
```python
install_requires = [
    'tornado>=4.0,<5',
    # https://pagure.io/python-daemon/issue/18
    'pandas==0.18.*'
]
```

## What could go wrong?
You don't know if `setup.py` requires a very specific version of a dependency.

. . .

The actual current version of `pandas` is 0.24.0.  Installing this could end up breaking your existing `pandas`!

## Python Virtual Environments
A virtual environment modifies where your computer thinks the python libraries exist

## Conda
We have conda installed by default
```
$ conda create -n my-new-env python=3.6
$ source activate my-new-env # source not necessary when in anaconda prompt
```

## Conda packages
Conda has a built in package manager
```
(my-new-env) $ conda install my-new-env
```

. . .

`pip` also works inside of a conda environment
but it should be your second choice if you can't find the package
at https://anaconda.org/

## Sharing environments with other people
```
(my-new-env) $ conda env export > environment.yml
```

## Sharing environments with other people
```
# environment.yml
name: py37
channels:
  - defaults
dependencies:
  - blas=1.0=mkl
  - ca-certificates=2018.12.5=0
  - certifi=2018.11.29=py37_0
  - intel-openmp=2019.1=144
```

## What I don't like
- `conda` environments can be hard to port across environments

. . .

- I have to remember to update my environment file

. . .

-----
I never remember to update my environment file

## Pipenv
"Python dev workflow for humans"

Pipenv aims to combine virtual environments and package management into one command
https://pipenv.readthedocs.io/en/latest/

[PyCon 2018 Presentation](https://www.youtube.com/watch?v=GBQAKldqgZs)

## Pipenv
Instead of `pip install ...` replace it with `pipenv install`
```
$ pipenv install pandas
Creating a virtualenv for this project…
```

## Pipenv
Pipenv will create two new files:

- Pipfile- the equivalent of a requirements.txt file
- Pipfile.lock- the last known working configuration of all dependencies

## Pipenv

- Destroys versions of your virtual environment as new packages are installed 
- Re-checks the dependency matrix to ensure packages play nice together

## Pipenv
- Windows is a "first-class citizen"

. . .

- Very specific, declarative environment file

. . . 

- Requirement files are updated every time, automatically


# Configuration Environments

## Your code can't keep secrets

"A litmus test for whether an app has all config correctly factored out of the code is whether thecodebase could be made open source at anymoment, without compromising any credentials."
- The Twelve-Factor App

## Configuration files
Builtin [`configparser`](https://www.pythonforbeginners.com/code-snippets-source-code/how-to-use-configparser-in-python)
```python
[bug_tracker]
url         = http://localhost:8080/bugs/
username    = dhellmann
password    = SECRET
```

## Configuration files
JSON files
```json
{
    "bug_tracker": {
        "url"       : "http://localhost:8080/bugs/",
        "username"  : "dhellmann",
        "password"  : "SECRET"
    }
}
```

## Configuration files
YAML files
```yaml
bug_tracker:
  url: http://localhost:8080/bugs
  username: dhellman
  password: SECRET
```
Best of both worlds?

## What is wrong with the example?

## Environment variables
Before running we could do this:
```bash
$ export USERNAME=dhellman
$ export PASSWORD=SECRET
```

## Environment variables
and then...
```python
from os import environ
USERNAME = environ.get('USERNAME')
PASSWORD = environ.get('PASSWORD')
```

. . .

but that's annoying every time we want to run code!


## .env (dotenv) files
`.env` files can be used for storing sensitive information 
```
USERNAME=dhellman
PASSWORD=SECRET
```
\* Note the lack of spaces. These are like bash variables!

## Env files
If you're using `pipenv`, it will automatically load the `.env` into  your environment when you `pipenv run ...` or `pipenv shell`.

You could skip the `bash` part above. 

## How to make it even better?

## Back to the configuration!
```yaml
# Note to future self: 
# these are stored in a .env file that is not version controlled
# don't get mad at the computer!
secret_vars:
  - USERNAME
  - PASSWORD
```
Note the env variables we want to import in our config

## Then add one step of processing on our config file
```python
import yaml
with open('my_config.yaml', 'r') as f:
    config = yaml.load(f)
    config['bug_tracker']['secret_vars'] = {var : environ.get(var) for var in config['secret_vars']}
```


## Things that should never be hard coded
* Absolute references to anything
* Credentials

<!-- 
o	versioning code
o	versioning data
o	config files (and version them)
o	testing  -->
